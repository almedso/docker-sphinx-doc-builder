FROM ubuntu:latest

ENV DEBIAN_FRONTEND noninteractive

# Update apt-get sources AND install stuff
RUN apt-get update && apt-get install -y -q \
  build-essential \
  inkscape \
  pandoc \
  plantuml \
  python3-sphinx \
  python3-pip \
  fonts-texgyre \
  latexmk \
  rsync \
  tex-common \
  tex-gyre \
  texlive-base \
  texlive-binaries \
  texlive-fonts-recommended \
  texlive-latex-base \
  texlive-latex-extra \
  texlive-latex-recommended \
  texlive-lang-german \
  texlive-pictures \
  texlive-plain-generic \
  texlive-xetex

# install stuff using python
RUN pip3 install \
  sphinxcontrib-traceability \
  sphinxcontrib-sadisplay \
  sphinxcontrib-actdiag \
  sphinxcontrib-aafig \
  sphinxcontrib-bibtex==0.3.0 \
  sphinxcontrib-needs \
  sphinxcontrib-plantuml \
  sphinxcontrib-programoutput \
  sphinxcontrib-email \
  sphinxcontrib-blockdiag \
  sphinxcontrib-tikz \
  rst2pdf \
  sphinx-rtd-theme \
  aafigure \
  actdiag

# copy scripts and makefiles
COPY build-doc.sh svg-to-pdf.mk /

RUN mkdir -p /documents
WORKDIR /documents/

ENTRYPOINT [ "/build-doc.sh" ]
