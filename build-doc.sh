#! /bin/bash

help () {
    cat <<EOHELP
This container has the sphinx environment installed

is sensitive about the following paramters

html - build html documentation
pdf - build pdf documentation
all - build html and pdf documentation

no parameter will shart a bash shell

Environment variables injected
------------------------------

SOURCE_DIR:
    where the conf.py is expected. If not set . and docs is tried.
BUILD_DIR:
    where the output is created. If not set build is used.
EOHELP
}


init_vars () {
   # if source dir not set try '.' and './docs'
   if [ -z ${SOURCE_DIR+x} ] ; then
      if [ -f "./conf.py" ] ; then
         SOURCE_DIR='.'
      fi
      if [ -f "docs/conf.py" ] ; then
         SOURCE_DIR='docs'
      fi
   fi
   if [ ! -f "${SOURCE_DIR}/conf.py" ]; then
      echo "ERROR: no conf.py found - set SOURCE_DIR correctly"
   fi
   BUILD_DIR=${BUILD_DIR:-"build"}
}

install_missing () {
   # install potentially additional prerequisites that are defined
   # in a requirements file in source directory
   # install in user context (--user)
   if [ -f requirements.txt ] ; then pip3 install --user -r requirements.txt ; fi
}


build_html () {
   sphinx-build -b html ${SOURCE_DIR} ${BUILD_DIR}/html
}

build_pdf () {
   # convert svgs to pdf since latex is short on processing svgs
   # this is done in an extra makefile since the sphinx make files aregenerated
   # note: the svg-to-pdf makefile needs to know about the directories where
   # svgs are located
   make -f /svg-to-pdf.mk images

   # build latex / pdf
   sphinx-build  -b latex $SOURCE_DIR $BUILD_DIR/pdf
   make -C $BUILD_DIR/pdf

   # clean up the temporarily generated pdfs
   make -f /svg-to-pdf.mk clean
}


main () {
    init_vars

    echo "INFO command line: $@"
    echo "INFO environent: SOURCE_DIR=${SOURCE_DIR}"
    echo "INFO environent: BUILD_DIR=${BUILD_DIR}"

    case ${1} in
    html)
        build_html
        ;;
    pdf)
        build_pdf
        ;;
    all)
        build_html
        build_pdf
        ;;
    --help|-?|help)
        help
        cleanup_and_exit
        ;;
    *)
        # act as shell if nothing is provided
        exec "$@"
        ;;
    esac
}

main "$@"
