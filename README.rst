===============================================
Builder Container using sphinx for html and pdf
===============================================

This project specifies a docker container that contains
all tools to create

* html output
* pdf output

of a **Sphinx based** documentation source.

Features of the sphinx based doc-toolchain container:

* proper pdf and svg image support
* bibtex support
* plantuml support
* traceability format
* bootstrap theme, read the docs theme

Create and publish
==================

Run create the image and upload it to docker hub with
Note the *1* is the version to produce. This number need to be increased
for every new image build.

.. code ::

   cd this-project-directory
   docker build -t almedso/sphinx-doc-builder:latest .
   docker tag almedso/sphinx-doc-builder:latest almedso/sphinx-doc-builder:1
   docker push almedso/sphinx-doc-builder:1


It is important to use the image name exactly as given above.

.. note ::

   This section is only important for the maintainers of this docker images.
   Users of the image do not have to take care and just use the latest version
   from the docker hub.


Usage
=====

Use it via
.. code ::

   cd <your_directory>
   docker run --rm --volume $PWD:/documents/ --user $(id -u):$(id -g) almedso/sphinx-doc-builder html|pdf|all


if the conf.py file is located in a directory different from project root e.g. in *docs* folder
then inject the source dir via

.. code ::

   cd <your_directory>
   docker run --rm --volume $PWD:/documents/ --user $(id -u):$(id -g) -e SOURCE_DIR=docs almedso/sphinx-doc-builder


whereby *your_directory* contains the an optional requirements.txt file and is the project root.


The sphinx project configuration file
**conf.py** and the index file and all the user files potentially in sub directories.
SOURCE_DIR points to the directory where the **conf.py** file is located or is empty if
the this file is in *your_directory* itself.

.. note ::

   * The internal volume to work on is */documents*
   * The internal volume is prepulated with some additional data.

The output will be placed in *your_directory*/build folder.
in html and pdf sub folders respectively.

.. note ::

   if the generation is not started with --user $(id -u) option i.e. as root user
   the produced results in build folder will be owned by host root user!!!


PDF Generation
==============

Images are converted to pdf first and integrated as pdf to latex.
This is currently the only way that works safely.

